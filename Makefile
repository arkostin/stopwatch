# Makefile - makefile for stopwatch project
# Alexander Kostin, arkostin01@gmail.com

CC="gcc"
CFLAGS="-Werror"

make				:				stopwatch.o
	$(CC) $(CFLAGS) -o sw stopwatch.o

stopwatch.o :				stopwatch.c
	$(CC) $(CFLAGS) -c stopwatch.c

clean				:
	rm -rf sw *.o

debug				:
	$(CC) $(CFLAGS) -g stopwatch.c
	gdb ./a.out
	rm -f a.out
