# Stopwatch
A program to measure the average execution time of a given program. Runs a program a number of times and returns the average time taken to execute (Default is CPU time, wall time can be specified with the -w flag).
## Build
Requirements: `gcc`<br>
Run `make` to generate the 'sw' binary.
## Usage
Syntax: `sw <flags> <program>`
- flags (All are optional)
  - \-t \<trials\>: Specifies number of trials to do (Default: 10).
  - \-w: Uses wall time (Default: CPU time).
  - \-o: Hides output of target program.
  - \-v: Verbose mode - prints out the time of each individual trial.
- program: Name of file or command to execute. If the target file or command has arguments, it must be encapsulated along with them in quotes (e.g. `sw "ls -al"`).
