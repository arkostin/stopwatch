// stopwatch.c - main source file for stopwatch program
// Alexander Kostin, arkostin01@gmail.com

#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

double get_time(bool get_wall_time);

int main(int argc, char **argv)
{
  bool quiet = false, verbose = false, wall_time = false;
  int32_t trials = 10;
  char flag;

  if (argc < 2)
  {
    fprintf(stderr, "Usage: sw <flags> <program>\n");
    exit(EXIT_FAILURE);
  }

  while ((flag = getopt(argc, argv, "t:wov")) > 0)
  {
    switch (flag)
    {
      case 't':
        trials = atoi(optarg);
        if (trials <= 0)
        {
          fprintf(stderr, "Error: number of trials must be greater than 0\n");
          exit(EXIT_FAILURE);
        }
        break;
      case 'w':
        wall_time = true;
        break;
      case 'o':
        quiet = true;
        break;
      case 'v':
        verbose = true;
        break;
      default:
        fprintf(stderr, "Error: invalid flag %c\n", flag);
        break;
    }
  }

  if (argv[optind] == NULL)
  {
    fprintf(stderr, "Usage: sw <flags> <program>\n");
    exit(EXIT_FAILURE);
  }

  if (quiet)
  {
    strcat(argv[optind], " > /dev/null 2>&1");
  }

  struct timespec total_t, t1, t2;
  memset(&total_t, 0, sizeof(struct timespec));

  clockid_t clock_type = wall_time ? CLOCK_REALTIME : CLOCK_PROCESS_CPUTIME_ID;

  for (int i = 0 ; i < trials ; i++)
  {
    if(clock_gettime(clock_type, &t1) == -1)
    {
      fprintf(stderr, "Error: could not get time, errno %d\n", errno);
      exit(EXIT_FAILURE);
    }

    system(argv[optind]); // Execute program

    if(clock_gettime(clock_type, &t2) == -1)
    {
      fprintf(stderr, "Error: could not get time, errno %d\n", errno);
      exit(EXIT_FAILURE);
    }

    total_t.tv_sec += (t2.tv_sec - t1.tv_sec);
    total_t.tv_nsec += (t2.tv_nsec - t1.tv_nsec);

    if (verbose)
    {
      printf("trial %d: %fs\n", (i + 1), ((double) (t2.tv_nsec - t1.tv_nsec) / 1000000000));
    }
  }

  printf("average time over %d trials: %fs\n", trials, total_t.tv_sec + ((double) (total_t.tv_nsec) / 1000000000) / trials);
  
  return 0;
}
